# Reflection Based Command Line Parsing in C++

##### Small proof-of-concept for reflective command line parsing

The idea for this command line parser comes from this [C++Now Talk](https://www.youtube.com/watch?v=VMuML6vLSus).

## Example

Lets say your application has three options that has to provide. The option `foo` takes an `int`, the option `bar` takes an `double` and is optional and the option `baz` is simply a switch. To use `parse_command_line` you need to provide an struct

```C++
#include "parse_command_line.hh"
struct options {
  BOOST_HANA_DEFINE_STRUCT(
      options,
      (int, foo),
      (std::optional<double>, bar),
      (bool, baz)
  );
};
```

The struct needs to be a `Hana` struct to enable reflection over the members.

`parse_command_line` is the simply invoked as

```C++
#include "parse_command_line.hh"
#include <iostream>

int main(int argc, char** argv) {
  auto opt = sv::parse_command_line<options>(argc, argv);

  std::cout << opt.foo << '\n';
  if (opt.bar.has_value())
    std::cout << opt.bar.value() << '\n';
  std::cout << std::boolalpha << opt.baz << '\n';
}
```

The command line for calling this program is

```bash
./example --foo n [--bar x] [--baz]
```

## Todo

* Print help message
* Default arguments
* Positional arguments
* Custom `switch` type to disambiguate `--baz` from `--baz=(0|1)`
* Parse config file
  * Parse nested structs from nested configurations as in `*.ini` files

## Dependencies

* C++17
* [Boost.Hana](https://www.boost.org/doc/libs/1_70_0/libs/hana/doc/html/index.html)
* [Boost.ProgramOptions](https://www.boost.org/doc/libs/1_70_0/doc/html/program_options.html)
