#include "parse_command_line.hh"

#include <iostream>

struct options {
  BOOST_HANA_DEFINE_STRUCT(         //
      options,                      //
      (int, foo),                   //
      (std::optional<double>, bar), //
      (bool, baz)                   //
  );
};

int main(int argc, char** argv) {
  auto x = sv::parse_command_line<options>(argc, argv);

  std::cout << x.foo << '\n';
  if (x.bar.has_value())
    std::cout << x.bar.value() << '\n';
  std::cout << std::boolalpha << x.baz << '\n';
}
