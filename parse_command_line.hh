#ifndef PARSE_COMMAND_LINE_PARSE_COMMAND_LINE_HH_1561304388393621939_
#define PARSE_COMMAND_LINE_PARSE_COMMAND_LINE_HH_1561304388393621939_

#include <boost/hana.hpp>
#include <boost/program_options.hpp>

#include <optional>
#include <type_traits>

namespace sv {

namespace detail {
template <typename T>
struct is_std_optional : std::bool_constant<false> {};
template <typename T>
struct is_std_optional<std::optional<T>> : std::bool_constant<true> {};
template <typename T>
constexpr bool is_std_optional_v = is_std_optional<T>::value;
template <typename T>
struct get_optional_type {
  using type = T;
};
template <typename T>
struct get_optional_type<std::optional<T>> {
  using type = T;
};
template <typename T>
using get_optional_type_t = typename get_optional_type<T>::type;
} // namespace detail


template <typename T>
T parse_command_line(int argc, char** argv) {
  namespace hana = boost::hana;
  namespace po   = boost::program_options;

  T options;

  po::options_description options_desc("Options");

  hana::for_each(hana::keys(options), [&](auto key) {
    auto name   = hana::to<const char*>(key);
    auto& value = hana::at_key(options, key);
    using type  = std::decay_t<decltype(value)>;
    if constexpr (std::is_same_v<type, bool>) {
      options_desc.add_options()(name, po::bool_switch());
    }
    else if constexpr (detail::is_std_optional_v<type>) {
      options_desc.add_options()(
          name, po::value<detail::get_optional_type_t<type>>());
    }
    else {
      options_desc.add_options()(name, po::value<type>()->required());
    }
  });

  po::variables_map vm;
  po::store(po::command_line_parser(argc, argv).options(options_desc).run(),
            vm);
  po::notify(vm);

  hana::for_each(hana::keys(options), [&](auto key) {
    auto name   = hana::to<const char*>(key);
    auto& value = hana::at_key(options, key);
    using type  = std::decay_t<decltype(value)>;
    if constexpr (detail::is_std_optional_v<type>) {
      if (vm.count(name) > 0) {
        value = vm[name].template as<detail::get_optional_type_t<type>>();
      }
    }
    else {
      value = vm[name].template as<type>();
    }
  });

  return options;
}
} // namespace sv
#endif // PARSE_COMMAND_LINE_PARSE_COMMAND_LINE_HH_1561304388393621939_
